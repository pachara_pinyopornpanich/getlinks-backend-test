# README #
Backend developer test
create by Pachara Pinyopornpanich

run app.js
port: 4000
---------------------------------------------------

----- Register -----
![register.png](https://bitbucket.org/repo/akkELj4/images/2705897602-register.png)

POST /register
Ex: { "username": "ABC", "password": "1234"}

Response:{message: 'register success'} or {message: 'register fail'}

----- Login -----
![login.png](https://bitbucket.org/repo/akkELj4/images/3244455998-login.png)

POST /login
Ex: { "username": "ABC", "password": "1234"}

Response:{message: 'success',token: token}
Ex: {message: 'success',token: "eyJhbGciOiJIUzI1NiJ9.bW9vaw.4PVxXAmw8SyjMXUBgl4wtSKmH2ttv-7qkji5b8MkLo0"}

GET /login?token=....
Ex: localhost:4000/login?token:eyJhbGciOiJIUzI1NiJ9.bW9vaw.4PVxXAmw8SyjMXUBgl4wtSKmH2ttv-7qkji5b8MkLo0

Response:{message: 'login success'} or {message: 'login fail'}