function checkUsername(username) {
    userData = [];
    for (index in memberMock) {
        userData.push(memberMock[index].username)
    }

    return userData.indexOf(username) < 0 ? 0 : 1
};

exports.putMember = function(req, res) {
    registerData = req.body;

    if (checkUsername(registerData.username) == 0) {
        registerData.password = crypto.createHash('md5').update(registerData.password).digest("hex");

        memberMock.push(registerData);

        fs.writeFile(process.cwd() + '/memberData.json', JSON.stringify(memberMock), function(err) {
            return res.json({
                status: 200,
                message: 'register success'
            });
        });
    } else {
        return res.json({
            status: 401,
            message: 'register fail: Someone already has that username'
        });


    }

};
