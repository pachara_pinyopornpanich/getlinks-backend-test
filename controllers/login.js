function findMember(username) {
    for (index in memberMock) {
        if (memberMock[index].username == username) {
            return memberMock[index]
        }
    }

    return 0
};

function encryptPassword(password) {
    encryptPasswords = crypto.createHash('md5').update(password).digest("hex");

    return encryptPasswords

};

exports.getMember = function(req, res) {
    var token = req.query.token;

    jwt.verify(token, 'GetLinkskeys', function(err, decoded) {
        if (token == undefined || !token || findMember(decoded) == 0) {
            return res.json({
                message: 'login fail'
            });
        }

        return res.json({message: 'login success'});
    });

};

exports.status = function(req, res) {
    var userlogin = req.body;

    if (userlogin == undefined || !userlogin) {
        return res.json({
            status: 404,
            message: 'user not found'
        });
    }

    if (!findMember(userlogin.username)) {
        return res.json({
            status: 401,
            message: 'incorrect username'
        });
    }

    if (findMember(userlogin.username).password != encryptPassword(userlogin.password)) {
        return res.json({
            status: 401,
            message: 'incorrect password'
        });

    }

    var token = jwt.sign(userlogin.username, 'GetLinkskeys');

    return res.json({
        status: 200,
        message: 'success',
        token: token
    });
};
