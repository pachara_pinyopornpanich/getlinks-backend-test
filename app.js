var restify = require('restify');
fs = require('fs');
memberMock = require('./memberData.json');
crypto = require('crypto');
jwt = require('jsonwebtoken');


var app = restify.createServer({
    name: 'GetLinks member',
    versions: ['1.0.0', '2.0.0'],
});
app.use(restify.queryParser());
app.use(restify.bodyParser());

var controllers = {};
var controllers_path = process.cwd() + '/controllers'

fs.readdirSync(controllers_path).forEach(function(file) {
    controllers[file.split('.')[0]] = require(controllers_path + '/' + file)
})

app.get("/login", controllers.login.getMember);
app.post("/login", controllers.login.status);
app.post("/register", controllers.register.putMember);


app.listen(4000, function() {
    console.log('%s at %s', app.name, app.url);
});
